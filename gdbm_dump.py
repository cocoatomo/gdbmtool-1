#!/bin/env python
# -*- coding: utf-8 -*-

import sys
import base64
import gdbm

def gdbm_dump_to_file(db, fp):
    k = db.firstkey()
    while k is not None:
        fp.write(base64.b64encode(k) + "\n")
        fp.write(base64.b64encode(db[k] + "\n"))
        k = db.nextkey(k)

if __name__ == "__main__":
    argc = len(sys.argv)
    if argc < 2 or argc > 3:
        print 'Usage: gdbm_dump.py DB_FILE [FILE]'
        exit()

    dbname = sys.argv[1]
    filename = None
    if argc == 3:
        filename = sys.argv[2]

    fp = None
    if filename is None or filename == "-":
        fp = sys.stdout
    else:
        fp = open(filename, "w")

    db = gdbm.open(dbname)

    gdbm_dump_to_file(db, fp)
    fp.close()

